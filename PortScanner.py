#!/usr/bin/python

import socket
import subprocess
import sys
from datetime import datetime 

#clear the console
subprocess.call('clear',shell=True)
remoteServer = raw_input("Server name for scan: ")
remoteServerIP = socket.gethostbyname(remoteServer)
sPort = int(raw_input("Start Port: "))
ePort = int(raw_input("End Port: "))

#counter
NumOfScannedPorts = 0

print "-" * 60
print "Scanning server with IP: [",remoteServerIP,"]"
print "-" * 60

#staring time
sTime=datetime.now()
#the real job
try:
    for port in range(sPort,ePort):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(1)
        NumOfScannedPorts += 1
        result = sock.connect_ex((remoteServerIP, port))
        if result == 0:
            print "Port {}:\t Open".format(port)
        sock.close()

except KeyBoardInterrupt:
    print "Ctrl+c"
    sys.exit()

except socket.gaierror: 
    print "Wrong DNS"
    sys.exit()

except socket.error:
    print "Host is unreachable"
    sys.exit()

#ending time
eTime = datetime.now()

tTime = eTime - sTime

print "Scanned ",NumOfScannedPorts," port(s) in ",tTime

exit(0)
