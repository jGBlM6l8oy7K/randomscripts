#!/bin/bash

SECONDS=0
#Colors
reset=`tput sgr0`
green=`tput setaf 2`
red=`tput setaf 1`
random=`tput setaf 6`

export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

#update
echo "${green}Updating system...${reset}"
sudo apt update 
sudo apt upgrade

#checking reboot-requried
if [ -f /var/run/reboot-required ]; then
	echo "${red}Server needs reboot! Reboot your server and start this script again!${reset}"
	exit 0
fi	

#dependecies
echo "${green}Installing dependecies${reset}"
sudo apt-get install postgresql ca-certificates curl openssh-server postfix -y

#installing gitlab
echo "${green}Installing GitLab${reset}"
cd /tmp
echo "${random}"
sudo curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh
chmod +x script.deb.sh
./script.deb.sh
echo "${reset}"
if [ -$? -ne 0 ]; then
	echo "${red}Something went wrong with the GitLab script! Exiting...${reset}"
	exit 1
fi
sudo apt install gitlab-ce

#firewall
echo "${green}Setting up firewall${reset}"
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
sudo ufw allow ssh 
sudo ufw reload
sudo ufw enable

#configurating
echo "${green}Configurating GitLab!${red}"
echo "${green}Do you have domain?${red}"
read DOMAINANW

if [[ "$DOMAINANW" == "yes" || "$DOMAINANW" == "Y" || "$DOMAINANW" == "y" || "$DOMAINANW" == "YES" ]]; then
	#yes case -> https + domain
	echo "${green}What is your domain?${reset}"
	read DOMAINNAME
	sed -i "s|external_url 'http:\/\/gitlab.example.com'|external_url 'https://$DOMAINNAME'|g"  /etc/gitlab/gitlab.rb
	echo "${green}What is you email (for encrypting)${reset}"
	read EMAIL
	echo "letsencrypt['enable'] = true" >> /etc/gitlab/gitlab.rb
	echo "letsencrypt['contact_emails'] = ['$EMAIL']" >> /etc/gitlab/gitlab.rb
else
	#no case -> http + IP
	echo "${blue}IPs: "
	sudo cat /etc/network/interfaces | grep address | awk '{print $2}'
	echo "${green}What is the IP of the server?${reset}"
	read IPofSE
	sed -i "s|external_url 'http:\/\/gitlab.example.com'|external_url 'http://$IPofSE'|g"  /etc/gitlab/gitlab.rb
fi

#Starting gitlab
echo "${green}Starting GitLab${reset}"
sudo gitlab-ctl reconfigure 

exit 0
