#ifndef STATICTEXT_HPP_INCLUDED
#define STATICTEXT_HPP_INCLUDED

#include "oswidget.hpp"

class staticText : public oswidget{
private:
    std::string szoveg;
public:
    staticText(int a,int b,std::string s);
    void draw() const;
};

#endif // STATICTEXT_HPP_INCLUDED
