#ifndef LEGORDULO_HPP_INCLUDED
#define LEGORDULO_HPP_INCLUDED

#include <vector>
#include "graphics.hpp"
#include "oswidget.hpp"

///legordulo resz hpp

class legor : public oswidget {
private:
    std::vector<std::string> lehet;              /// lehetseges valasztasok
    bool success;                                /// sikeresen talalt-e választast
    int melyiket;                                /// melyik elemet kell kijelezni
    int butselIs;                                /// legordulo menuhoz allapot jelzes
    int mettol;                                  /// honnan mutasson 6 elemet a menu
    int meddig;                                  /// hany elemet akarunk (min 3)
    int hany;                                    /// hany elemet tudunk megjelentiteni a listan
public:
    legor(double a, double b,double c,double d,double han,std::vector<std::string> alma); /// konstruktor (kezdo x, kezdo y, hossz, magassag, mennyi elemet akarunk (min 3))
    virtual void clrscr() const;                 /// képernyotorlo, pontosabban csak a widget helyet torli le feherrel
    virtual bool butsel(int mx, int my) const;   /// akkor ad vissza igaz erteket, ha a lenyilo gombra kattintanak
    void addnew(std::string a);                  /// uj valasztasi lehetoség hozzaadasa
    virtual void textdraw() const;               /// a kirajzolashoz segedfuggveny, a megfelelo szoveget irja ki
    virtual void draw() const;                   /// az alap kirajzolása szoveg és legorulo menu nekul
    virtual void down();                         /// legordulo menu kirajzolo
    virtual void handler(genv::event ev);                  /// kezelo fv.
    virtual bool dbut(int mx, int my) const;     /// akkor ad vissza igaz erteket, ha a legordulo menuben a lefele gombra nyomtunk
    virtual bool ubut(int mx, int my) const;     /// akkor ad vissza igaz erteket, ha a legordulo menuben a felfele gombra nyomtunk
    virtual void wordselected(int mx,int my);    /// eldonti, hogy melyik szot valasztottuk ki a menubol
    virtual void gorget(genv::event ev);                 /// gorgetest kezeli
    int reporter() const;                        /// most az, hogy melyiket vasztottuk getter
    bool listsel(int mx,int my) const;           /// akkor ad vissza igaz erteket, ha a legordulo menun van a kurzor
    virtual bool canlist() const;                /// akkor ad vissza igaz erteket, ha lehetesges a lefele gordites azaz tobb mint 6 elem van a listán
    virtual bool everythingSelected(int mx,int my) const;   ///focus rajta van
};


#endif // LEGORDULO_HPP_INCLUDED
