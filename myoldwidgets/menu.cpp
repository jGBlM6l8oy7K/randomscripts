#include "graphics.hpp"
#include "menu.hpp"
#include "oswidget.hpp"
#include "legordulo.hpp"
#include "szam.hpp"
#include "gomb.hpp"
#include "iroswidget.hpp"
#include "MapElement.hpp"

using namespace std;
using namespace genv;

///menu cpp

void menu::draw(){  ///kirajzolas
    gout<<move_to(0,0)<<color(255,255,255)<<box(XX,YY);
    gout<<move_to(XX/2-30,50)<<color(0,0,0)<<text("Amoba 1.0")<<move_to(20,YY-20)<<text("Created by : Bohm Tamas Zsombor - SQNCGV")<<move_to(XX-400,YY-20)<<text("https://github.com/BohmTamas/BevProg2_3.beadando");
    gout<<move_to(XX/2+60,YY-665)<<color(0,0,0)<<text("Name of Player 2")<<move_to(XX/2-240,YY-665)<<text("Name of Player 1"); ///felirat
    gout<<move_to(XX/2-35,YY-555)<<color(0,0,0)<<text("Map size");      ///map gomb
    gout<<move_to(XX/2-145,YY-405)<<color(0,0,0)<<text("Number of the same units in row to win");      ///hányas amőba
    gout<<move_to(XX/2-50,YY-305)<<color(0,0,0)<<text("Picking time (s)");
}

bool menu::handler(event ev,int &time,int &mennyi, int &hany,vector<vector<MElement*>> &p,string &a, string &b,int &malac){ ///kezelo true ha nem tortent semmi ->false indul a jatek
    if (ev.pos_x!=0 && ev.pos_y!=0) {       ///focus kezelo
        mx = ev.pos_x;
        my = ev.pos_y;
    }
    draw();
    g->handler(ev);     ///kezdogomb
    s1->kezelo(ev,mx,my);   ///ido beallito widget
    s2->kezelo(ev,mx,my);   ///hanyas amoba gomb
    l->kezel(ev);           ///map meret valaszto
    i1->kezelo(ev,mx,my);   ///elso jatekos neve X
    i2->kezelo(ev,mx,my);   ///masodik jatekos neve O
    if (g->getStatus()){    ///ha lenyomjuk a gombot
            gout<<move_to(0,0)<<color(255,255,255)<<box(XX,YY); ///feher hatter
            time=s1->getInt();  ///ido beolvasasa
            mennyi=l->reporter();   ///mekkora map merete beolvasas
            hany=s2->getInt();      ///hanyszor hanyas beolvasa
            for (int i = 0; i <mennyi; i++) {   ///palya legeneralasa
                        vector<MElement*> row;
                            for (int j = 0; j < mennyi; j++) {
                                row.push_back(new MElement(50+j*50,50+i*50));  ///SOR -> I ||| OSZLOP -> J
                                row[row.size()-1]->setBack();           ///alaphelyzetbe allitas
                            }
                        p.push_back(row);
            }
            a=i1->getString();  ///elso jatekos neve beolvasas
            b=i2->getString();  ///masodik jatekos neve beolvasas
            if (a.length()==0) a="Player 1";    ///ha nincs jatekos nev
            if (b.length()==0) b="Player 2";    ///ha nincs jatekos nev
            if (hany>mennyi) hany=mennyi;       ///ha a map merete kisebb mint ahanyas amoba
            malac=0;        ///konrtoll resz malac = 0 -> jatek | malac = 1 -> dontetlen | malac = 2 -> valaki nyert
            return false;
    }
    return true;
}
