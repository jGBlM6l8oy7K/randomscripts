#ifndef CHECHKBOX_HPP_INCLUDED
#define CHECHKBOX_HPP_INCLUDED

#include "graphics.hpp"
#include "oswidget.hpp"

class cBox : public oswidget{
private:
    bool status;
public:
    cBox(double a, double b, double c);
    virtual void draw() const;
    virtual void drawX() const;
    virtual void handler(genv::event ev);
    bool getStatus() const;
};


#endif // CHECHKBOX_HPP_INCLUDED
