#include "graphics.hpp"
#include "oswidget.hpp"
#include "szam.hpp"
#include <sstream>
#include <vector>

using namespace genv;
using namespace std;

///szamos widget cpp

szamos::szamos(double x,double y,double xx, double yy, int mi,int ma) : oswidget(x,y,xx,yy){  ///konstruktor
    maxsz = ma;         ///maximum
    minsz = mi;         ///minimum
    szam = 0;           ///megjelenitett szam
    if (_sx < 40) _sx=40;   /// nem engedi tul rovidnek lenni
    if (_sy < 40 ) _sy=40;  /// nem engedi tul "alacsonynak" lenni
    if (szam > maxsz) szam = maxsz;      /// ha a max kisebb mint 0, akkor a maximumot veszi fel
    if (szam < minsz) szam = minsz;      /// ha a min nagyobb mint 0 akkor a minimumot veszi fel
}

void szamos::draw() const{  ///grafikai panel
    gout<<move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<move_to(_x+2,_y+2)<<color(255,255,255)<<box(_sx-4,_sy-4); /// keret
    { /// szam kiirása
    stringstream ss;
    string sss;
    ss<<szam;
    ss>>sss;
    gout<<move_to(_x+10,_y + _sy/2 + 5)<<color(0,0,0)<<text(sss);
    }
    gout << move_to(_x + _sx - 12 ,_y)<<color(0,0,0)<<line(0,_sy)<<move_to(_x+_sx-9,_y+15)<<line_to(_x+_sx-7, _y+10)<<line_to(_x+_sx-5,_y+15)<<move_to(_x+_sx-12,_y+_sy/2)<<line(10,0)<<move_to(_x+_sx-9,_y+_sy-15)<<line_to(_x+_sx-7,_y+_sy-10)<<line_to(_x+_sx-5,_y+ _sy-15); /// gombok
}

void szamos::handler(event ev){  ///kezeles
        if (ev.pos_x!=0 || ev.pos_y!=0){
            mx=ev.pos_x;
            my=ev.pos_y;
        }
        if (selected(mx,my)){
            if (ev.button==btn_left){ ///gombok kezelese kattintas
                if (ev.pos_x>(_x+_sx-12) && ev.pos_x<(_x+_sx)&&ev.pos_y < (_y+_sy/2) && ev.pos_y>_y) szam++; ///novelő - egyesevel novekedik
                if (ev.pos_x>(_x+_sx-12) && ev.pos_x<(_x+_sx)&&ev.pos_y > (_y+_sy/2) && ev.pos_y<_y+_sy) szam--; ///csokkento - egyevesel csokken
            }
            if (ev.keycode == key_up|| ev.button == btn_wheelup)szam++;  ///noveles billentyuvel
            if (ev.keycode == key_down|| ev.button == btn_wheeldown) szam--; /// csokkentes billentyuvel
            if (ev.keycode==key_pgdn) szam-=10;             ///pagedown -> tizesevel csokken
            if (ev.keycode == key_pgup) szam+=10;           ///pageup -> tizesevel novekszik
            if (szam>maxsz) szam = maxsz; /// maximum korlatozas
            if (szam<minsz) szam = minsz; /// minimum korlatozas
        }
        draw(); /// kirajzolas
}

int szamos::getInt() const{     ///szam getter
    return szam;
}
