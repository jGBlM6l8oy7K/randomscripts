#include "graphics.hpp"
#include "window.hpp"
#include "gomb.hpp"
#include "iroswidget.hpp"
#include "staticText.hpp"

using namespace std;
using namespace genv;

const int XX=800;
const int YY=600;

void window::loop(){
    gout.open(800,600);
    for (int j = 0;j<8;j++) i.push_back(new iros(50,50+j*60,100,50,""));
    for (int i = 0;i<8;i++) g.push_back(new gomb(160,50+i*60,50,25,"->"));
    while (gin>>ev && ev.keycode!=key_escape){
        if (ev.pos_x!=0 || ev.pos_x!=0){
            mx = ev.pos_x;
            my=ev.pos_y;
        }
        for (int j = 0;j<8;j++){
            i[j]->handler(ev);
            g[j]->handler(ev);
        }
        gout<<refresh;
    }
}
