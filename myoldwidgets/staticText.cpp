#include "graphics.hpp"
#include "oswidget.hpp"
#include "staticText.hpp"

using namespace std;
using namespace genv;

staticText::staticText(int a,int b,string s) : oswidget(a,b,gout.twidth(s)+25,gout.cascent()+gout.cdescent()+20){
    szoveg=s;
}

void staticText::draw() const{
    gout << move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<move_to(_x+2,_y+2)<<color(255,255,255)<<box(_sx-4,_sy-4)<<move_to(_x+ 10,_y+_sy/2 +5)<<color(0,0,0)<<text(szoveg);
}
