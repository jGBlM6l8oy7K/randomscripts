#ifndef OSWIDGET_HPP_INCLUDED
#define OSWIDGET_HPP_INCLUDED

#include "graphics.hpp"

///oswidget hpp

class oswidget{
protected:
    double _x,_y,_sx,_sy; /// kezdo X,kezdo Y, hossz, magassag
public:
    oswidget(double a, double b,double c, double d); ///konstruktor
    virtual bool selected(int mx, int my) const; /// az eger az objektum folott van-e
    void handler();
    void draw() const;
};

#endif // OSWIDGET_HPP_INCLUDED
