#include "oswidget.hpp"
#include "graphics.hpp"

using namespace std;
using namespace genv;

///oswidget cpp

oswidget::oswidget(double a,double b,double c, double d){ ///konstruktor
        _x=a;
        _y=b;
        _sx=c;
        _sy=d;
}

bool oswidget::selected(int mx, int my) const{ /// az eger az objektum folott van-e
    bool res = false;
    if (mx>_x && mx <(_x+_sx) && my> _y && my<(_y+_sy)) res = true;
    return res;
}
