#ifndef SZAM_HPP_INCLUDED
#define SZAM_HPP_INCLUDED

#include "graphics.hpp"
#include "oswidget.hpp"

///szam hpp

class szamos : public oswidget{
private:
    int szam;                                                       /// megjelenitett szam
    int maxsz, minsz;                                               /// intervallum max és min
    int mx,my;
public:
    szamos(double x, double y,double xx,double yy, int mi,int ma);  /// konstruktor (kezdo X, kezdo Y, hossz, magassag, minimum, maximum)
    virtual void draw() const;                                           /// rajzolo fv
    virtual void handler(genv::event ev);                                    /// kezelo fv
    int getInt() const;
};


#endif // SZAM_HPP_INCLUDED
