#include "graphics.hpp"
#include "oswidget.hpp"
#include "cBox.hpp"

using namespace std;
using namespace genv;

cBox::cBox(double a, double b, double c) : oswidget(a,b,c,c){
    bool status = false;
}

void cBox::drawX() const{
    gout<<move_to(_x,_y)<<color(0,64,0)<<line_to(_x+_sx,_y+_sy)<<move_to(_x,_y+_sy)<<line_to(_x+_sx,_y);
}

void cBox::draw() const{
    gout<<move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<color(255,255,255)<<move_to(_x+2,_y+2)<<box(_sx-4,_sy-4);
    if (status) drawX();
}

void cBox::handler(event ev){
    if (selected(ev.pos_x, ev.pos_y)&& ev.button == btn_left) status = !status;
    draw();
}

bool cBox::getStatus() const{
    return status;
}
